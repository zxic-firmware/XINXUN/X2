#!/bin/sh

main()
{
    echo timer > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/trigger
    echo 250 > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/delay_on
    echo 250 > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/delay_off

    echo 0 > /sys/devices/platform/leds-gpio.1/leds/modem_4g_led/brightness

    echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/brightness
    echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/brightness

}

main